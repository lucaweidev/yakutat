#include <cmath>
#include <omp.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

#include "MPI_tools.hpp"
#include "mpi/SparseMatrix/ELL_SparseMatrix.hpp"
#include "Resolution.hpp"


using std::cout;
using std::endl;
using std::vector;
using std::fstream;
using std::ofstream;



// create A matrix of Ax = B
void createPressureMatrix
   (
    // ======================================================== //

    yakutat::SparseMatrixELL<double> &matA,

    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs,
    MPI_tools P_MPI


    // ======================================================== //
    )
{
    // ======================================================== //

    size_t icel;
    int shift1 = 1;
    int shift2 = nz-2*gCells;
    int shift3 = (nz-2*gCells)*(ny-2*gCells);
    double aa, bb, cc, csr_coef;


    const int caliceltot = (nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells);

    vector<vector<double> > coef(6,vector<double>(caliceltot));
    vector<double> dia_coef(caliceltot);
    

    // ======================================================== //


    icel = 0;
    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k )
            {  

                

                if(i==gCells)
                {
                    aa = -1/iDx[i]/Dxs[i-1];
                }
                else if(i==(nx-gCells-1))
                {
                    aa = -1/iDx[i]/Dxs[i];
                }
                else
                    aa = 0;

                if(j==gCells)
                {
                    bb = -1/iDy[j]/Dys[j-1];
                }
                else if(j==(ny-gCells-1))
                {
                    bb = -1/iDy[j]/Dys[j];
                }
                else 
                    bb = 0;

                if(k==gCells)
                {
                    cc = -1/iDz[k]/Dzs[k-1];
                }
                else if(k==(nz-gCells-1))
                {
                    cc = -1/iDz[k]/Dzs[k];
                }
                else
                    cc = 0;
                
                dia_coef[icel] = 1/iDx[i]/Dxs[i] + 1/iDx[i]/Dxs[i-1] + 1/iDy[j]/Dys[j] + 1/iDy[j]/Dys[j-1] + 1/iDz[k]/Dzs[k] + 1/iDz[k]/Dzs[k-1] \
                                + aa + bb + cc;
                // matA.set(icel, icel, dia_coef[icel]);
                ++icel;
                
            }
        }
    }
    
    #pragma omp parallel for schedule(guided) 
    for(int i=P_MPI.start; i<P_MPI.end+1; ++i)
    {
        matA.set(i, i, dia_coef[i]);
    }

   

    // coo define coeficient of pressure matrix. define value, expect for dia value

    icel = 0;
    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k )
            {  

                // coef[0][icel] = -1/iDx[i]/Dxs[i-1]; // AREA INTERPOLATION AT POINT CENTER (i-1,j,k)
                // coef[1][icel] = -1/iDx[i]/Dxs[i];   // AREA INTERPOLATION AT POINT CENTER (i+1,j,k)
                // coef[2][icel] = -1/iDy[j]/Dys[j-1]; // AREA INTERPOLATION AT POINT CENTER (i,j-1,k)
                // coef[3][icel] = -1/iDy[j]/Dys[j];   // AREA INTERPOLATION AT POINT CENTER (i,j+1,k)
                // coef[4][icel] = -1/iDz[k]/Dzs[k-1]; // AREA INTERPOLATION AT POINT CENTER (i,j,k-1)
                // coef[5][icel] = -1/iDz[k]/Dzs[k];   // AREA INTERPOLATION AT POINT CENTER (i,j,k+1)

                if(i!=gCells)
                {
                    coef[0][icel] = -1/iDx[i]/Dxs[i-1]; // AREA INTERPOLATION AT POINT CENTER (i-1,j,k)
                    // matA.set(icel, icel-shift3, coef[0][icel]);
                }
                else if(i!=(nx-gCells-1))
                {
                    coef[1][icel] = -1/iDx[i]/Dxs[i];   // AREA INTERPOLATION AT POINT CENTER (i+1,j,k)
                    // matA.set(icel, icel+shift3, coef[1][icel]);
                }

                if(j!=gCells)
                {
                    coef[2][icel] = -1/iDy[j]/Dys[j-1]; // AREA INTERPOLATION AT POINT CENTER (i,j-1,k)
                    // matA.set(icel, icel-shift2, coef[2][icel]);

                }
                else if(j!=(ny-gCells-1))
                {
                    coef[3][icel] = -1/iDy[j]/Dys[j];   // AREA INTERPOLATION AT POINT CENTER (i,j+1,k)
                    // matA.set(icel, icel+shift2, coef[3][icel]);
                }

                if(k!=gCells)
                {
                    coef[4][icel] = -1/iDz[k]/Dzs[k-1]; // AREA INTERPOLATION AT POINT CENTER (i,j,k-1)
                    // matA.set(icel, icel-shift1, coef[4][icel]);
                }
                else if(k!=(nz-gCells-1))
                {
                    coef[5][icel] = -1/iDz[k]/Dzs[k];   // AREA INTERPOLATION AT POINT CENTER (i,j,k+1)
                    // matA.set(icel, icel+shift1, coef[5][icel]);
                }

                
        
                ++icel;
            }
        }
    }

    #pragma omp parallel for schedule(guided)
    for(int i=P_MPI.start; i<P_MPI.end+1; ++i)
    {
        if(i-shift3 >= 0)
            matA.set(i, i-shift3, coef[0][i]);

        if(i+shift3 < caliceltot)
            matA.set(i, i+shift3, coef[1][i]);

        if(i-shift2 >= 0)
            matA.set(i, i-shift2, coef[2][i]);

        if(i+shift2 < caliceltot)
            matA.set(i, i+shift2, coef[3][i]);
        
        if(i-shift1 >= 0)
            matA.set(i, i-shift1, coef[4][i]);
        
        if(i+shift1 < caliceltot)
            matA.set(i, i+shift1, coef[5][i]);
    }
        
   

    


}


void createBMatrix
   (
    // ======================================================== //

    // MPI_tools &U_MPI, const int master, int myid, int nproc,
    double dt,  MPI_tools U_MPI, int myid, int nproc,
    vector<vector<double> > &NEIBcell,
    vector<vector<double> > &U_star,

  
    vector<double> &B_vector, 


    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs

    // ======================================================== //
    
    )
{
    // ======================================================== //

    size_t caliceltot = (nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells);

    double ddt = 1.0 / dt;
    
    double div;

    int xm, ym, zm, xp, yp, zp;

    size_t icel, cnt;

    // ======================================================== //

    B_vector.clear();
    B_vector.resize(caliceltot, 0);

    // coo define B matrix 

    icel = 0;
    // cnt = 0;
    cnt = (ny-2*gCells) * (nz-2*gCells) * (U_MPI.start-gCells);
    for(size_t i = U_MPI.start; i < U_MPI.end+1; ++i ) 
    {
        
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k )
            {  
                icel = i*nz*ny + j*nz + k;

                xm  = NEIBcell[0][icel];
                xp  = NEIBcell[1][icel];

                ym  = NEIBcell[2][icel];
                yp  = NEIBcell[3][icel];

                zm  = NEIBcell[4][icel];
                zp  = NEIBcell[5][icel];

                div = -ddt*( ( U_star[0][xp] - U_star[0][xm] )/(2*iDx[i])+ \
							 ( U_star[1][yp] - U_star[1][ym] )/(2*iDy[j])+ \
							 ( U_star[2][zp] - U_star[2][zm] )/(2*iDz[k])  );

              
							
                B_vector[cnt] = div;

                ++cnt;
            }
        }
    }

}






double product
    (
    // ======================================================== //
    vector<double> &a,
    vector<double> &b,
    MPI_tools P_MPI
   

    // ======================================================== //
    )
{
    // ======================================================== //
    double c = 0.0;
    double c_ = 0.0;
    // ======================================================== //
    #pragma omp parallel for schedule(guided) reduction(+:c)
    for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
    {
        c += a[i] * b[i];
    }

    MPI_Allreduce((void *)&c, (void *)&c_, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    

    return c_;
}




vector<double> BiCGSTAB
    (
    // ======================================================== //
    MPI_tools P_MPI, int myid, int nproc, const int master, size_t num_threads,

    double zeta, int itmax,

    yakutat::SparseMatrixELL<double> &matA,

    vector<double> &B_vector


    // ======================================================== //
    
    )
{
    
    // ======================================================== //

    double alpha,beta,norm0,norm,sum,scal,norm1,norm2,omega,rho1,rho2;

    double norm_, norm0_;

    size_t ik, icel, itag;

    const int caliceltot = B_vector.size();
    
    vector<double> p(caliceltot);
    
    vector<double> x(caliceltot);
	
    vector<double> r(caliceltot);

    vector<double> r2(caliceltot);

    vector<double> v(caliceltot);

    vector<double> ss(caliceltot);

    vector<double> t(caliceltot);

    // ======================================================== //


    norm0 = product(B_vector, B_vector, P_MPI);
    
    norm0 = sqrt(norm0); 

    p = matA * x; 

    

    #pragma omp parallel for schedule(guided) 
    for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
        r[i] = B_vector[i] - p[i];

    #pragma omp parallel for schedule(guided)
    for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
        r2[i] = r[i];

    rho1  = 1;
    alpha = 1;
    omega = 1;

    #pragma omp parallel for schedule(guided)
    for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
    {
        v[i] = 0;
        p[i] = 0;
    }

    norm = 0;

    norm = product(r, r, P_MPI);


    norm = sqrt(norm) / norm0;



    

    ik = 0;

    while(norm>zeta && ik<itmax)
    {
        rho2 = product(r2, r, P_MPI);

        beta = (rho2/rho1) * (alpha/omega);

        #pragma omp parallel for schedule(guided)
        for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)  
            p[i] = r[i] + beta * (p[i] - omega * v[i]);


        p = MPI_Collect(p, P_MPI, caliceltot, 100, myid, nproc);
       
        v = matA * p; 


        alpha = rho2 / product(r2, v, P_MPI);


        #pragma omp parallel for schedule(guided)
        for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
            ss[i] = r[i] - alpha * v[i];


        ss = MPI_Collect(ss, P_MPI, caliceltot, 101, myid, nproc);
        
        t = matA * ss; 

        omega = product(t, ss, P_MPI) / product(t, t, P_MPI);

        

        #pragma omp parallel for schedule(guided)
        for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
            x[i] += alpha * p[i] + omega * ss[i];
        
        #pragma omp parallel for schedule(guided)
        for(size_t i=P_MPI.start; i<P_MPI.end+1; ++i)
            r[i] = ss[i] - omega * t[i];

        

        rho1 = rho2;

        norm = 0;

        norm = product(r, r, P_MPI);

        norm = sqrt(norm) / norm0;


        ++ik;
    }

    


    

    if(myid == master)
    {
        cout << "===================================================================="     << endl;
        cout << "Num Of Iter = " << ik << ", Residual = " << norm << endl;

    }


  

    return x;

}






















