#ifndef _VARIABLES_
#define _VARIABLES_

    #include <string>
    // Physical variable
    // ======================================================== //
    double Re = 100.0;
    double dt = 1.0e-3;
    double time = 0.0;
    size_t nstep = 20;
    size_t isto = 1000;
    double zeta = 1.0e-4;
    double itmax = 3000;
    //non-uniform 0, uniform 1 
    std::string Grid_switch = "uniform";
    // ======================================================== //

    // mesh
    // ======================================================== //
    double dySml, dy;
    double dxSml, dx;
    double dzSml, dz;
    // ======================================================== //

    // other
    // ======================================================== //
    size_t istep;
    double dRe;
    // ======================================================== //

    

#endif