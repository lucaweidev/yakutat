#ifndef _PRECONDITIONER_INCLUDE_
#define _PRECONDITIONER_INCLUDE_

#include <vector>
#include "mpi/SparseMatrix/ELL_SparseMatrix.hpp"
using std::vector;


yakutat::SparseMatrixELL<double> diagonal_preconditioner
    (
    MPI_tools P_MPI, int myid, int nproc,
    yakutat::SparseMatrixELL<double> &matA
    );
#endif
