
#ifndef _GRIDDER_INCLUDED_
#define _GRIDDER_INCLUDED_
#include <string>
#include <vector>

using std::vector;

void gridder
(
    double GridderXc, double GridderYc, double GridderZc, 
    double lxSml, double lySml, double lzSml, double dx,
    double dy, double dz, double dxSml, double dySml, double dzSml,
    std::string Grid_switch, int myid,

    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs

);


#endif

