#ifndef __MPI_TOOLS_H__
#define	__MPI_TOOLS_H__

#include <vector>

using std::vector;

    struct MPI_tools
    {
        int start, end, count;
        vector<int> start_list, end_list, count_list;
    };


    

    void MPI_division
    (
    int length,
    int StartPoint,
    int &start, 
    int &end, 
    int &count,
    vector<int> &MPIstart,
    vector<int> &MPIend,
    vector<int> &MPIcount,
    int myid,
    int nproc
    );

    vector<double> MPI_Collect
    (
    vector<double> x,
    MPI_tools MPI,
    size_t total_length,
    size_t itag,
    int myid,
    int nproc
    );

#endif