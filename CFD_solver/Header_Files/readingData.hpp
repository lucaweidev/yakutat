
#ifndef _READINGDATA_
#define _READINGDATA_
#include <string>
#include <vector>

using std::vector;


void readingData
(
    double &dx, double &dy, double &dz, 
    const double lx, const double ly, const double lz, 
    const double lxSml, const double lySml, const double lzSml,
    std::string Grid_switch, 
    const int nx, const int ny, const int nz,
    const double nxSml, const double nySml, const double nzSml, 
    double &dxSml, double &dySml, double &dzSml,
    double Re, double &dRe
);

void NEIBceller
(
    vector<vector<double> > &NEIBcell

);

void readingMatrixData
(
    vector<double> &x,
    vector<double> &P,
    MPI_tools U_MPI

);

#endif
